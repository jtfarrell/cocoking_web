from django.contrib import admin
from django.db import models
from django import forms

from .models import *

class ArticleAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
    list_display = ['aboutUs', 'aboutCoconuts']

    class Media:
        js = ('assets/ckeditor/ckeditor.js',)

        css = {
            "all": ('assets/css/ckeditor_fix.css',)
        }

class DetailsAdmin(admin.ModelAdmin):
    forfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})}, }
    list_display = ['mission', 'orders_and_deliveries', 'our_coconuts', 'made_with_love']

    class Media:
        js = ('assets/ckeditor/ckeditor.js',)

        css = {
            "all": ('assets/css/ckeditor_fix.css',)
        }

class StockistAdmin(admin.ModelAdmin):
    forfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})}, }
    list_display = ['body']

    class Media:
        js = ('assets/ckeditor/ckeditor.js',)

        css = {
            "all": ('assets/css/ckeditor_fix.css',)
        }



admin.site.register(About, ArticleAdmin)
admin.site.register(Details, DetailsAdmin)
admin.site.register(Stockist, StockistAdmin)
