from __future__ import absolute_import

from django.shortcuts import render
from django.template.context_processors import csrf

from .models import *


def home(request):

    stockist = Stockist.objects.all()
    about = About.objects.get(pk=1)
    details = Details.objects.get(pk=1)

    args = {}
    args.update(csrf(request))
    args['stockist'] = stockist
    args['about'] = about
    args['details'] = details

    return render(request, 'index.html', args)

def home_greek(request):

    stockist = Stockist.objects.all()

    args = {}
    args.update(csrf(request))
    args['stockist'] = stockist

    return render(request, 'index_greek.html', args)
