# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_aboutgreek_detailsgreek_stockistgreek'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AboutGreek',
        ),
        migrations.DeleteModel(
            name='DetailsGreek',
        ),
        migrations.DeleteModel(
            name='StockistGreek',
        ),
    ]
