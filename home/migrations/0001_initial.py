# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import home.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('aboutUs', models.TextField()),
                ('aboutCoconuts', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Details',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mission', models.TextField()),
                ('orders_and_deliveries', models.TextField()),
                ('our_coconuts', models.TextField()),
                ('made_with_love', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Stockist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('body', models.TextField()),
                ('thumbnail', models.FileField(null=True, upload_to=home.models.get_upload_file_name, blank=True)),
                ('web_link_url', models.URLField(null=True, blank=True)),
            ],
        ),
    ]
