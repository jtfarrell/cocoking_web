from django.db import models

from time import time

def get_upload_file_name(instance, filename):
    return "img/%s_%s" % (str(time()).replace('.', '-'), filename)

class About(models.Model):
    aboutUs = models.TextField()
    aboutCoconuts = models.TextField()

class Details(models.Model):
    mission = models.TextField()
    orders_and_deliveries = models.TextField()
    our_coconuts = models.TextField()
    made_with_love = models.TextField()

class Stockist(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    thumbnail = models.FileField(upload_to=get_upload_file_name, blank=True, null=True)
    web_link_url = models.URLField(blank=True, null=True)
